from django.urls import path
from django.contrib import admin
from . import views

app_name = 'Story07App'

urlpatterns = [
    path('', views.index, name='index'),
]