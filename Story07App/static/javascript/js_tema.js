function lightmode(){
    document.getElementById("bg").style.background = "url(/static/images/light.jpg)"
    document.getElementById("bg").style.backgroundPosition = "center"
}

function darkmode(){
    document.getElementById("bg").style.background = "url(/static/images/dark.jpg)"
    document.getElementById("bg").style.backgroundPosition = "center"
}